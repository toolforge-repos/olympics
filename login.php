<?php
require_once 'vendor/autoload.php';
use MediaWiki\OAuthClient\ClientConfig;
use MediaWiki\OAuthClient\Consumer;
use MediaWiki\OAuthClient\Client;


$ini = parse_ini_file( getcwd().'/login.ini' );
if ( $ini === false ) {
	header( "HTTP/1.1 $errorCode Internal Server Error" );
	echo 'The ini file could not be read';
	exit(0);
}
if (!isset( $ini['consumerKey'] ) ||
	!isset( $ini['consumerSecret'] )
) {
	header( "HTTP/1.1 $errorCode Internal Server Error" );
	echo 'Required configuration directives not found in ini file';
	exit(0);
}

$endpoint = 'https://en.wikipedia.org/w/index.php?title=Special:OAuth';
$consumerKey = $ini['consumerKey'];
$consumerSecret = $ini['consumerSecret'];

$conf = new ClientConfig( $endpoint );

$conf->setConsumer( new Consumer( $consumerKey, $consumerSecret ) );

$client = new Client( $conf );

// Step 1 = Get a request token
list( $next, $token ) = $client->initiate();

// Step 2 - Have the user authorize your app. Get a verifier code from
// them. (if this was a webapp, you would redirect your user to $next,
// then use the 'oauth_verifier' GET parameter when the user is redirected
// back to the callback url you registered.
header("Location: $next");
print "Enter the verification code:\n";
$fh = fopen( 'php://stdin', 'r' );
$verifyCode = trim( fgets( $fh ) );
echo $verifyCode;

// Step 3 - Exchange the token and verification code for an access
// token
$accessToken = $client->complete( $token,  $verifyCode );

// You're done! You can now identify the user, and/or call the API with
// $accessToken

// If we want to authenticate the user
$ident = $client->identify( $accessToken );
echo "Authenticated user {$ident->username}\n";

// Do a simple API call
echo "Getting user info: ";
echo $client->makeOAuthCall(
    $accessToken,
    'https://en.wikipedia.org/w/api.php?action=query&meta=userinfo&uiprop=rights&format=json'
);

$client->setExtraParams( $apiParams ); // sign these too

echo $client->makeOAuthCall(
    $accessToken,
    'https://en.wikipedia.org/w/api.php',
    true,
    $apiParams
);
?>