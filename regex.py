import re
import sys
import urllib2
import calendar
import json

name_to_num = {name: num for num, name in enumerate(calendar.month_name)
               if num}
url = sys.argv[1]
# url = "http://web.archive.org/web/20140705041929/"
#       "http://www.sochi2014.com:80/en/athlete-sally-mayara-da-silva"
response = urllib2.urlopen(url)
page_source = response.read()
rawname = re.findall("<h2 itemprop=\"name\">([^<]*)", page_source)
rawnames = rawname[0].split(" ")
fullname = ""

for rawname in rawnames:
    capitalname = rawname[1:]
    lowername = capitalname.lower()
    name = rawname[0][0]+lowername
    fullname += " %s" % name
fullname = fullname[1:]

tasks = ['<a itemprop="affiliation" href=[^>]*>([^<]*)',
         '<span>Birthday:<\/span>&nbsp;([^<]*)',
         '<span>Height:<\/span>&nbsp;([^< ]*)',
         '<span>Weight:<\/span>&nbsp;([^< ]*)',
         '<span>Place of Birth:<\/span>&nbsp;([^<]*)',
         'class="sport small [^>]*>([^<]*)'
         ]

num = 1
numt = 0
while num != 7:
    everything = re.findall(tasks[numt], page_source)
    everything = [x for x in everything if x != ''][0]
    if everything == "-":
        everything = ""
    if num == 1:
        country = everything
    elif num == 2:
        birthdate = everything
    elif num == 3:
        height = everything
    elif num == 4:
        weight = everything
    elif num == 5:
        birthplace = everything
    elif num == 6:
        event = everything
    num += 1
    numt += 1

response = urllib2.urlopen("https://restcountries.eu/rest/v2/name/%s"
                           % country)
demonym = json.loads(response.read())[0]['demonym']
birthseperate = birthdate.split(" ")
birthyear = birthseperate[2]
birthmonthname = birthseperate[1]
birthmonthnumber = name_to_num[birthmonthname]
birthday = birthseperate[0]
birthplaceraw = birthplace.replace(",", "").split(" ")
birthplaceraw.append(birthplaceraw.pop(0))
birthplace = ""

for place in birthplaceraw:
    placerawclean = place[1:]
    placeclean = placerawclean.lower()
    birthplaceclean = place[0][0] + placeclean
    birthplace += " %s" % birthplaceclean

birthplace = re.split("( )", birthplace[1:])
birthplacenocountry = birthplace[-3] + ','
birthplace.insert(len(birthplace)-3, birthplacenocountry)
birthplace.remove(birthplace[3])
birthplace = ''.join(birthplace)

eventsdict = {'Alpine Skiing': 'alpine ski racer',
              'Biathlon': 'biathlete',
              'Bobsleigh': 'sportsperson',
              'Cross-country': 'skier',
              'Curling': 'curler',
              'Figure Skating': 'figure skater',
              'Freestyle skiing': 'skier',
              'Ice Hockey': 'ice hockey player',
              'Luge': 'sportsperson',
              'Nordic Combined': 'sportsperson',
              'Short Track': 'short track speed skater',
              'Speed Skating': 'speed skater',
              'Skeleton': 'sportsperson',
              'Ski Jumping': 'skier',
              'Snowboard': 'sportsperson'}

# yes, I know my names are great
othereventsdict = {'Alpine Skiing': '[[alpine ski racer]]',
                   'Biathlon': '[[biathlete]]',
                   'Bobsleigh': '[[bobsledder]]',
                   'Cross-country': '[[cross country skier]]',
                   'Curling': '[[curling|curler]]',
                   'Figure Skating': '[[figure skater]]',
                   'Freestyle skiing': '[[freestyle skier]]',
                   'Ice Hockey': '[[ice hockey]] player',
                   'Luge': '[[luge|luger]]',
                   'Nordic Combined': '[[Nordic combined]] skier',
                   'Short Track': '[[short track speed skater]]',
                   'Speed Skating': '[[speed skater]]',
                   'Skeleton': '[[skeleton racer]]',
                   'Ski Jumping': '[[ski jumper]]',
                   'Snowboard': '[[snowboarder]]'}


defaulttext = "\
{{{{Infobox {11}\
\n| name = {0}\
\n| nationality = {{{{flag|{1}}}}}\
\n| birth_date = {{{{Birth date and age|df=yes|{2}|{3}|{4}}}}}\
\n| birth_place = {5}\
\n| height = {{{{convert|{6}|m|ftin|abbr=on}}}}\
\n| weight = {{{{convert|{7}|kg|lb|abbr=on}}}}\
\n}}}}\
\n'''{0}''' (born {4} {8} {2}) is a {12} Olympic {9}.<ref name=BioRio>{{{{\
cite web|url={10}|title={0} |work=Pyeongchang2018 |access-date={{{\
{subst:Currentdaymonth}}}}\
{{{{subst:Currentyear}}}}}}</ref>\
\n\
\n== References ==\
\n{{{{reflist}}}}\
\n\
\n[[Category:{2} births]]\
\n[[Category:Living people]]".format(fullname, country, birthyear,
                                     birthmonthnumber, birthday, birthplace,
                                     height, weight, birthmonthname,
                                     othereventsdict[event], url,
                                     eventsdict[event], demonym)
result = {'fullname': fullname, 'defaulttext': defaulttext}
print json.dumps(result)
