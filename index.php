<?php
session_start();
if( !isset($_SESSION['username']) ){
	header( 'Location: http://127.0.0.1/login.php' ) ;
}

if( isset($_SESSION['flags']) ){
	header( 'Location: https://127.0.0.1/error.php?e=editperm' );
 }
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Olympics creation tool</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link href="css/index.css" rel="stylesheet">
		<!-- Icon at the top -->
		<link rel="icon" type="image/png" href="favicon.png">

    </head>

    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <a class="navbar-brand" href="https://127.0.0.1/olympics/">Olympics creation</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        </nav>
      <form action="athlete.php" method="post" class="form-inline justify-content-center">
    <div class="text-center form">
      <input class="form-control" name="athleteurl" type="url" placeholder="Full URL of athlete" style="width:500px" />
	  <button type="submit" class="btn btn-success">Proceed</button>
    </div>
      </form>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.2.1/jquery.slim.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/popper.js/1.11.0/popper.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    </body>

    </html>