<?php
session_start();
if( !isset($_SESSION['username']) ){
	header( 'Location: http://tools.wmflabs.org/olympics/login.php' ) ;
}
$errorno = $_GET['e'];
if( $errorno == 'editperm' ) {
	$ereason = "Insufficient permissions";
	$ehelp = "You must be extended-confirmed to be able to use this tool";
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Olympics creation tool</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link href="css/index.css" rel="stylesheet">
		<!-- Icon at the top -->
		<link rel="icon" type="image/png" href="favicon.png">

    </head>

    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <a class="navbar-brand" href="https://tools.wmflabs.org/olympics/">Olympics creation</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        </nav>
        <div class="container">
            <div class='center'>
                <h1><?php echo $ereason; ?></h1>
                <h3><?php echo $ehelp; ?></h3>
                <br/>
                <hr/>
                <p><a href='/logout.php'>Logout &raquo;</a> &middot; <a href='https://en.wikipedia.org/w/index.php?title=User_talk:DatGuy&section=new'>File a bug report on my talk page &raquo;</a></p>
            </div>
        </div>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.2.1/jquery.slim.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/popper.js/1.11.0/popper.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    </body>

    </html>