<?php
session_start();
// Make an Edit
$editToken = json_decode( $client->makeOAuthCall(
    $accessToken,
    'https://en.wikipedia.org/wiki/api.php?action=tokens&format=json'
) )->tokens->edittoken;

$apiParams = array(
    'action' => 'edit',
    'title' => $_POST['title'],
    'summary' => 'Creating article about ' . $_POST['title'] . ' (using [[toolforge:olympics|a article creation tool)',
    'text' => $_POST['articletext'],
	'createonly' => 'true',
    'token' => $editToken,
    'format' => 'json'
);
?>