 <?php
 session_start();
 if( !isset($_SESSION['username']) ){
	 header( 'Location: http://127.0.0.1/login.php' ); 
}
 
 $redirecting = false;
 $rawwebsite = $website = "";

 if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
	 $rawwebsite = $_POST['athleteurl'];
	 if ( empty ( $rawwebsite ) ) {
		 $redirecting = true;	
	 } else {
		 $website = test_input( $rawwebsite );
		 // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
		 if ( !preg_match( "/\b(?:.+(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $website ) ) {
			 $redirecting = true;
		 } else {
			 if ( !preg_match("/^https?/", $website) ) { 
				$website = "http://".$website;
			 }
		 }
	 }
	 escapeshellarg( $website );
	 }
 function test_input( $data ) {
	 $data = trim( $data );
	 $data = stripslashes( $data );
	 $data = htmlspecialchars( $data );
	 return $data;
 }

 $result = json_decode(exec("python regex.py ".$website), true);
 
 if ( empty ( $result['defaulttext'] ) ) {
	 header("refresh:5; url=index.php");
	 $redirecting = true;
 }
 ?>
	<!DOCTYPE html>
    <html lang="en">

    <head>
        <title>Olympics creation tool</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
		<link href="css/athlete.css" rel="stylesheet">
		<!-- Icon at the top -->
		<link rel="icon" type="image/png" href="favicon.png">

    </head>	

    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <a class="navbar-brand" href="https://127.0.0.1/olympics/">Olympics creation</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        </nav>
	<form action="createpage.php" method="post" target="nullframe">
      <div class="form-group" style="text-align: center;">
	  	<?php
		if ( $redirecting ) {
			echo "<font size='18'> Page not functional for tool. Redirecting back...</font><br>";
		}
		?>
        <label for="title" class="control-label">Title (please change this if there are any errors, such as not having accent marks):</label>
        <input type="text" class="form-control" id="title" value="<?php echo $result['fullname']; ?>"/>
        <label for="articletext" class="control-label"><br>Article:</label>
        <textarea class="form-control" rows="20" id="articletext"><?php echo $result['defaulttext']; ?></textarea><br>
		<button type="submit" class="btn btn-success" align="center" style="padding-top: 10px">Create article</button>
      </div>
	  </form>
	  <iframe name="nullframe" style="display:none;"></iframe>




        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.2.1/jquery.slim.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/popper.js/1.11.0/popper.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    </body>

    </html>